#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "stb_image.h"

int main() {
    int width, height, bpp;
    uint8_t* rgb_image = stbi_load("font.png", &width, &height, &bpp, 3);

    if (rgb_image == NULL) {
        printf("Error in loading the image\n");
        return 1;
    }

    char chars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ .,!?0123456789/-+()%";
    int column = 0;
    int value = 0;
    int index = 0;

    printf("{\n");

    for (int x = 0; x < width; ++x) {
        if (column == 4) {
            column = 0;
            value = 0;
            index += 1;
            continue;
        }

        for (int y = 0; y < height; ++y) {
            int pixel_index = (y * width + x) * 3;
            value = value * 2 + (rgb_image[pixel_index] < 128 ? 1 : 0);
        }

        if (column == 3) {
            printf("  0x%04x, // %d \"%c\"\n", value, index, chars[index]);
        }

        column += 1;
    }

    printf("};\n");

    stbi_image_free(rgb_image);

    return 0;
}
