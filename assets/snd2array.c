#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("Usage: %s <filename>\n", argv[0]);
        return 1;
    }

    FILE *f = fopen(argv[1], "rb");
    if (!f) {
        printf("Error opening file\n");
        return 1;
    }

    int even = 1;
    int val = 0;
    int first = 1;
    int count = 0;

    printf("uint8_t sound[] = {\n");

    while (1) {
        unsigned char byte;
        if (fread(&byte, 1, 1, f) != 1) {
            break;
        }

        int quantized = byte / 16;

        if (even) {
            val = quantized << 4;
        } else {
            val = val | quantized;

            count += 1;

            if (!first) {
                printf(",");
            }

            if (count % 20 == 0) {
                printf("\n");
            }

            printf("0x%02x", val);

            first = 0;
        }

        even = !even;
    }

    printf("\n};\n");

    fclose(f);

    return 0;
}
